const defaults = require("../default");

const testObject = { name: "Bruce Wayne", age: 36, location: "Gotham" };
const defaultProps = { name: "Batman", universe: "DC" };

const result = defaults(testObject, defaultProps);

console.log(result);
