const mapObject = require("../mapObject");

function cb(value) {
  const newValue = value + 2;
  return newValue;
}

const testObject = { name: "Bruce Wayne", age: 36, location: "Gotham" };

const result = mapObject(testObject, cb);

console.log(result);
