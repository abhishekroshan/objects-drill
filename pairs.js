function pairs(obj) {
  const newArray = [];

  for (let property in obj) {
    let array = [];
    array.push(property);
    array.push(obj[property]);

    newArray.push(array);
  }
  return newArray;
}

module.exports = pairs;
