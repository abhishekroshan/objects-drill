function defaults(obj, defaultProps) {
  for (const prop in defaultProps) {
    if (obj[prop] === undefined) {
      obj[prop] = defaultProps[prop];
    }
  }
  return obj;
}

module.exports = defaults;
