function values(obj) {
  const valueArray = Object.values(obj);

  return valueArray;
}

module.exports = values;
