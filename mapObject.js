function mapObject(obj, cb) {
  const keys = Object.keys(obj);

  const newObject = {};

  keys.forEach((key) => {
    newObject[key] = cb(obj[key]);
  });

  return newObject;
}

module.exports = mapObject;
